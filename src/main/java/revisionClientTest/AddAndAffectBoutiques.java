package revisionClientTest;

import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import entities.Boutique;
import entities.Categorie;
import service.MyServiceRemote;

public class AddAndAffectBoutiques {

	public static void main(String[] args) throws NamingException {
		InitialContext cx= new InitialContext();
		Object obj= cx.lookup("/revision-ear/revision-ejb/MyService!service.MyServiceRemote");
		MyServiceRemote proxy= (MyServiceRemote) obj;
		List<Boutique> lb = new ArrayList<Boutique>();
		
		Boutique b1 = new Boutique("New collection",Categorie.Adulte);
		Boutique b2 = new Boutique("Sport City",Categorie.Sprot);
		Boutique b3 = new Boutique("Funny Kids",Categorie.Enfant);
		lb.add(b1);
		lb.add(b2);
		lb.add(b3);
		proxy.ajouterEtAffecterlisteBoutiques(lb, 1L);
	}

}
