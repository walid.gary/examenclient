package revisionClientTest;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import entities.CentreCommercial;
import service.MyServiceRemote;

public class AddCentre {

	public static void main(String[] args) throws NamingException {
		// TODO Auto-generated method stub
		InitialContext cx= new InitialContext();
		Object obj= cx.lookup("/revision-ear/revision-ejb/MyService!service.MyServiceRemote");
		MyServiceRemote proxy= (MyServiceRemote) obj;
		CentreCommercial c = new CentreCommercial("ArianaCenter","Ariana","centreA","centreA");
		
		proxy.ajouterCentre(c);
	}

}
