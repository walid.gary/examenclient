package revisionClientTest;

import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import entities.Boutique;
import entities.Client;
import entities.Genre;
import service.MyServiceRemote;

public class AddClientAndAffectBoutiques {

	public static void main(String[] args) throws NamingException {
		InitialContext cx= new InitialContext();
		Object obj= cx.lookup("/revision-ear/revision-ejb/MyService!service.MyServiceRemote");
		MyServiceRemote proxy= (MyServiceRemote) obj;
		List<Long> lb1 = new ArrayList<Long>();
		lb1.add(2L);
		lb1.add(3L);
		
		List<Long> lb2 = new ArrayList<Long>();
		lb2.add(1L);
		
		List<Long> lb3 = new ArrayList<Long>();
		lb3.add(1L);
		lb3.add(3L);
		
		Client c1 = new Client("Ahmed Ali",Genre.MASCULIN);
		Client c2 = new Client("Hend Salem",Genre.FEMININ);
		Client c3 = new Client("Sawsan Salhi",Genre.FEMININ);
		proxy.ajouterEtAffecterClientBoutiques(c1, lb1);
		proxy.ajouterEtAffecterClientBoutiques(c2, lb2);
		proxy.ajouterEtAffecterClientBoutiques(c3, lb3);
	}

}
