package revisionClientTest;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import entities.Categorie;
import entities.Client;
import entities.Genre;
import service.MyServiceRemote;

public class AfficherClientParCategorie {

	public static void main(String[] args) throws NamingException {
		InitialContext cx= new InitialContext();
		Object obj= cx.lookup("/revision-ear/revision-ejb/MyService!service.MyServiceRemote");
		MyServiceRemote proxy= (MyServiceRemote) obj;
		System.out.println("Liste : ");
		 for (Client c : proxy.listeDeClientsParCategorie(Categorie.Adulte)) {
			 System.out.println(c);
		 }

	}

}
