package revisionClientTest;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import entities.Boutique;

import service.MyServiceRemote;

public class AfficherListeBoutiques {

	public static void main(String[] args) throws NamingException {
		InitialContext cx= new InitialContext();
		Object obj= cx.lookup("/revision-ear/revision-ejb/MyService!service.MyServiceRemote");
		MyServiceRemote proxy= (MyServiceRemote) obj;
		for (Boutique c : proxy.listeBoutiques(1L)) {
			System.out.println(c);
		}
	}

}
