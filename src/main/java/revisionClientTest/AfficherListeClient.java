package revisionClientTest;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import entities.Client;
import service.MyServiceRemote;

public class AfficherListeClient {

	public static void main(String[] args) throws NamingException {
		InitialContext cx= new InitialContext();
		Object obj= cx.lookup("/revision-ear/revision-ejb/MyService!service.MyServiceRemote");
		MyServiceRemote proxy= (MyServiceRemote) obj;
		for (Client c : proxy.listeClients(1L)) {
			System.out.println(c);
		}
	}

}
