package revisionClientTest;

import java.util.HashMap;
import java.util.Map;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import entities.Genre;
import service.MyServiceRemote;

public class AfficherMap {

	public static void main(String[] args) throws NamingException {
		InitialContext cx= new InitialContext();
		Object obj= cx.lookup("/revision-ear/revision-ejb/MyService!service.MyServiceRemote");
		MyServiceRemote proxy= (MyServiceRemote) obj;
		
		Map<Genre, Long> eqs = new HashMap<Genre, Long>();
		eqs = proxy.statistiques();
		eqs.entrySet()
		  .stream()
		  .forEach(System.out::println);

	}

}
